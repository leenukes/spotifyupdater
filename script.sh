#!/bin/bash
#
# This script will download the .deb packages, unpack them and put the bits where they need to go.
#
# This does not have an uninstall element to it, nor does it keep it up to date. Sorry.
#
# Updated to just work and grab everything required rather then searching out a specific
# This needs to be ran as root, because it extracts files into /usr/bin and /usr/share which is root only. sudo should work.

# Check you're root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "Grabbing dependancies, say yes to this or Ctrl+C to cancel the script."

yum install binutils qt-x11 openssl nss nss-util nspr qtwebkit libpng-compat libXScrnSaver

mkdir -p packages/client/gnome
cd packages/client
lftp -e 'mget spotify-client-gnome-support*;exit' http://repository.spotify.com/pool/non-free/s/spotify/
ar xv spotify-client-gnome-support_*.deb data.tar.gz
tar -C / -xf data.tar.gz
cd gnome

if [ "$HOSTTYPE" == "x86_64" ]; then
lftp -e 'mget *amd64*;exit' http://repository.spotify.com/pool/non-free/s/spotify/
ar vx spotify-client_*_amd64.deb data.tar.gz
tar -C / -xf data.tar.gz
ln -s /lib64/libssl.so.10 /lib64/libssl.so.0.9.8
ln -s /lib64/libcrypto.so.10 //lib64/libcrypto.so.0.9.8
ln -s /lib64/libnss3.so /lib64/libnss3.so.1d
ln -s /lib64/libnssutil3.so /lib64/libnssutil3.so.1d
ln -s /lib64/libsmime3.so /lib64/libsmime3.so.1d
ln -s /lib64/libplc4.so /lib64/libplc4.so.0d
ln -s /lib64/libnspr4.so /lib64/libnspr4.so.0d

elif [ "$HOSTTYPE" == "i386" ]; then
lftp -e 'mget *i386*;exit' http://repository.spotify.com/pool/non-free/s/spotify/
ar vx spotify-client_*_i386.deb data.tar.gz data.tar.gz
tar -C / -xf data.tar.gz
ln -s /lib/libssl.so.10 /lib/libssl.so.0.9.8
ln -s /lib/libcrypto.so.10 /lib/libcrypto.so.0.9.8
ln -s /lib/libnss3.so /lib/libnss3.so.1d
ln -s /lib/libnssutil3.so /lib/libnssutil3.so.1d
ln -s /lib/libsmime3.so /lib/libsmime3.so.1d
ln -s /lib/libplc4.so /lib/libplc4.so.0d
ln -s /lib/libnspr4.so /lib/libnspr4.so.0d
fi

# for some reason Spotify is linked against specific libraries, but we can fake it with symlinks.

echo "Done, you should now be able to excute the spotify command succesfully. If not, something went wrong. Sorry"
